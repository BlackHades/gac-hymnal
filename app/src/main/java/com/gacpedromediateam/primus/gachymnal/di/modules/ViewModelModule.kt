package com.gacpedromediateam.primus.gachymnal.di.modules

import android.arch.lifecycle.ViewModel
import com.gacpedromediateam.primus.gachymnal.di.annotations.ViewModelKey
import com.gacpedromediateam.primus.gachymnal.ui.hymn.HymnViewModel
import com.gacpedromediateam.primus.gachymnal.ui.splash.InstallationViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import com.gacpedromediateam.primus.gachymnal.di.ViewModelFatory
import android.arch.lifecycle.ViewModelProvider




@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(HymnViewModel::class)
    internal abstract fun provideHymnalViewModel(hymnViewModel: HymnViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(InstallationViewModel::class)
    internal abstract fun provideInstallationViewModel(installationViewModel: InstallationViewModel): ViewModel


    @Binds
    internal abstract fun provideFactory(viewModelFactory: ViewModelFatory): ViewModelProvider.Factory
}