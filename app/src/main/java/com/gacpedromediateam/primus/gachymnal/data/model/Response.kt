package com.gacpedromediateam.primus.gachymnal.data.model

class Response<T:Any>{
    var status : Int = 0
    lateinit var message : String
    var data : T? = null
    override fun toString(): String {
        return "Response(status=$status, message='$message', data=$data)"
    }


}