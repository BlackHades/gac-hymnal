package com.gacpedromediateam.primus.gachymnal.ui.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView

import com.gacpedromediateam.primus.gachymnal.data.model.Verse
import com.gacpedromediateam.primus.gachymnal.R

import java.util.ArrayList

/**
 * Created by Primus on 7/11/2017.
 */

class HymnViewAdapter(context: Context, results: ArrayList<Verse>, private val lang: Int) : BaseAdapter() {

    private val mInflater: LayoutInflater = LayoutInflater.from(context)
    private var verseChar: ArrayList<Verse> = results



    override fun getCount(): Int {
        return verseChar.size
    }

    override fun getItem(position: Int): Any {
        return verseChar[position]
    }

    override fun getItemId(position: Int): Long {
        return verseChar.size.toLong()
    }

    override fun getView(position: Int, altView: View?, parent: ViewGroup): View {
        var convertView = altView
        val holder: ViewHolder
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.view_hymn_row, null)
            holder = ViewHolder()
            holder.ID = convertView!!.findViewById<View>(R.id.VerseID) as TextView
            holder.Title = convertView
                    .findViewById<View>(R.id.HymnVerse) as TextView
            convertView.tag = holder
        } else {
            holder = convertView.tag as ViewHolder
        }
        if (lang == 1) {
            holder.ID!!.text = verseChar[position].verse_id.toString()
            holder.Title!!.text = verseChar[position].english
        } else {
            holder.ID!!.text = verseChar[position].verse_id.toString()
            holder.Title!!.text = verseChar[position].yoruba
        }

        return convertView
    }

    internal class ViewHolder {
        var ID: TextView? = null
        var Title: TextView? = null
    }
}
