package com.gacpedromediateam.primus.gachymnal.ui.hymn

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.gacpedromediateam.primus.gachymnal.data.model.Hymn
import com.gacpedromediateam.primus.gachymnal.R

/**
 * Created by Primus on 7/10/2017.
 */

class HymnListAdapter(context: Context?, results: ArrayList<Hymn>, private var lang: Int?) : BaseAdapter(), Filterable {
    private val mInflater: LayoutInflater = LayoutInflater.from(context!!)
    private var mFilter: HymnFilter? = null
    private val TAG = "LISTADAPTER"
    private var hymnChar: ArrayList<Hymn> = results
    private var filteredData: ArrayList<Hymn> = results
    override fun getCount(): Int {
        return hymnChar.size
    }

    override fun getItem(position: Int): Any {
        return hymnChar[position]
    }

    override fun getItemId(position: Int): Long {
        return hymnChar.size.toLong()
    }

    override fun getView(position: Int, altView: View?, parent: ViewGroup): View {
        var convertView = altView
        val holder: ViewHolder
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.hymn_row, null)
            holder = ViewHolder()
            holder.ID = convertView!!.findViewById(R.id.txtId)
            holder.Title = convertView
                    .findViewById(R.id.txtTitle)
            holder.Fave = convertView.findViewById(R.id.imgFave)
            convertView.tag = holder
        } else {
            holder = convertView.tag as ViewHolder
        }

        if (lang == 1) {
            holder.ID!!.text = hymnChar[position].hymn_id.toString()
            holder.Title!!.text = hymnChar[position].english
        } else {
            holder.ID!!.text = hymnChar[position].hymn_id.toString()
            holder.Title!!.text = hymnChar[position].yoruba
        }
        if (hymnChar[position].favorite!! == 1) {
            holder.Fave!!.setImageResource(R.drawable.ic_action_fave)
        } else {
            holder.Fave!!.setImageResource(0)
        }
        return convertView
    }

    override fun getFilter(): Filter {
        if (mFilter == null) {
            mFilter = HymnFilter()
        }
        return mFilter!!
    }

    internal class ViewHolder {
        var ID: TextView? = null
        var Title: TextView? = null
        var Fave: ImageView? = null
    }

    fun changeLanguage(temp: Int) {
        if (lang != temp) {
            lang = temp
            notifyDataSetChanged()
        }
        //notifyDataSetChanged()

    }

    fun swap(hymns: ArrayList<Hymn>) {
        hymnChar = hymns
        filteredData = hymns
        notifyDataSetChanged()
    }

    private inner class HymnFilter : Filter() {

        override fun performFiltering(constraint: CharSequence?): Filter.FilterResults {
            val results = Filter.FilterResults()
            if (constraint != null && constraint.length > 0) {
                val filterList = ArrayList<Hymn>()
                for (i in filteredData.indices) {
                    if (lang == 1) {
                        if (filteredData[i].english!!.toUpperCase().contains(constraint.toString().toUpperCase()) || filteredData[i].hymn_id.toString().toUpperCase().contains(constraint.toString().toUpperCase())) {
                            val hh = Hymn(filteredData[i].hymn_id, filteredData[i].english, filteredData[i].yoruba, filteredData[i].favorite)
                            filterList.add(hh)
                        }
                    } else {
                        if (filteredData[i].yoruba!!.toUpperCase().contains(constraint.toString().toUpperCase()) || filteredData[i].hymn_id.toString().toUpperCase().contains(constraint.toString().toUpperCase())) {
                            val hh = Hymn(filteredData[i].hymn_id, filteredData[i].english, filteredData[i].yoruba, filteredData[i].favorite)
                            filterList.add(hh)
                        }
                    }
                }

                results.count = filterList.size
                results.values = filterList
            } else {
                results.count = filteredData.size
                results.values = filteredData
            }

            return results
        }

        override fun publishResults(charSequence: CharSequence, filterResults: Filter.FilterResults) {
            hymnChar = filterResults.values as ArrayList<Hymn>
            notifyDataSetChanged()
        }
    }

}
