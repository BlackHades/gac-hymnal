package com.gacpedromediateam.primus.gachymnal.ui.hymn

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.view.menu.MenuBuilder
import android.support.v7.view.menu.MenuPopupHelper
import android.support.v7.widget.PopupMenu
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.gacpedromediateam.primus.gachymnal.ui.contributors.ContributorActivity
import com.gacpedromediateam.primus.gachymnal.ui.favorites.FavoritesActivity
import com.gacpedromediateam.primus.gachymnal.ui.hymn.Fragments.AppFragment
import com.gacpedromediateam.primus.gachymnal.ui.hymn.Fragments.FeedbackFragment
import com.gacpedromediateam.primus.gachymnal.ui.hymn.Fragments.MainFragment
import com.gacpedromediateam.primus.gachymnal.data.source.local.AppPreference
import com.gacpedromediateam.primus.gachymnal.R
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_hymn.*
import java.util.*
import javax.inject.Inject

class HymnActivity : DaggerAppCompatActivity() {

    private val mainFragment = MainFragment()
    private val appFragment = AppFragment()
    private val feedbackFragment = FeedbackFragment()
    lateinit var prevMenuItem : MenuItem
    @Inject
    lateinit var appPreference: AppPreference
    var nightTheme = false
    val TAG = "HYMNACTIVITY"
    var title = arrayOf("Main", "Appendix", "Feedback")
    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                viewPager.currentItem = 0
                toolbar_title.text = title[0]
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_dashboard -> {
                viewPager.currentItem = 1
                toolbar_title.text = title[1]
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_notifications -> {
                viewPager.currentItem = 2
                toolbar_title.text = title[2]
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }


    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        if(appPreference.isNightMode()){
            nightTheme = true
            setTheme(R.style.AppThemeNight)
        }
        setContentView(R.layout.activity_hymn)
        setSupportActionBar(toolbar)
        supportActionBar?.title = ""
        if (appPreference.getLanguage() == 0) {
            title = arrayOf("Iwe Orin", "Akokun", "Esi")
        }
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        prevMenuItem = navigation.menu.getItem(0)
        setupViewPager(viewPager)
        toolbar_title.text = title[0]
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                prevMenuItem.isChecked = false

                navigation.menu.getItem(position).isChecked = true
                prevMenuItem = navigation.menu.getItem(position)
                toolbar_title.text = title[position]


            }

            override fun onPageScrollStateChanged(state: Int) {

            }
        })
    }

    private fun setupViewPager(viewPager: ViewPager) {
        val adapter = Adapter(supportFragmentManager)
        adapter.addFragment(mainFragment)
        adapter.addFragment(appFragment)
        adapter.addFragment(feedbackFragment)
        viewPager.offscreenPageLimit = 3
        viewPager.adapter = adapter
    }

    private class Adapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {
        private val mFragmentList = ArrayList<Fragment>()

        override fun getItem(position: Int): Fragment {
            return mFragmentList[position]
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFragment(fragment: Fragment) {
            mFragmentList.add(fragment)
        }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    @SuppressLint("NewApi", "RestrictedApi")
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_fave -> {
                startActivity(Intent(this, FavoritesActivity::class.java))
                overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out)
                return true
            }
            R.id.action_contributors -> {
                startActivity(Intent(this, ContributorActivity::class.java))
                overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out)
                //Log.e(TAG, "onOptionsItemSelected: Contributors Clicked")
                return true
            }
            R.id.action_language -> {
                var v : View = findViewById(R.id.toolbar)
                val popup = PopupMenu(this, v, Gravity.END)
                popup.menuInflater.inflate(R.menu.lang_menu, popup.menu)
                popup.setOnMenuItemClickListener {
                    var language = 1
                    when(it.itemId){
                        R.id.action_english -> language = 1

                        R.id.action_yoruba -> language = 0

                    }
                    appPreference.setLanguage(language)
                    setupViewPager(viewPager)
                    return@setOnMenuItemClickListener true
                }
                val menuHelper = MenuPopupHelper(this, popup.menu as MenuBuilder, v)
                menuHelper.setForceShowIcon(true)
                menuHelper.show()
            }
            R.id.action_night_mode ->{
                if(appPreference.isNightMode()){
                    appPreference.setNightMode(false)
                }else{
                    appPreference.setNightMode(true)
                }
                startActivity(intent)
                finish()
            }
            else -> return super.onOptionsItemSelected(item)
        }
        return true
    }

    override fun onResume(){
        super.onResume()
        if(appPreference.isNightMode() != nightTheme){
            startActivity(intent)
            finish()
        }

    }

}
