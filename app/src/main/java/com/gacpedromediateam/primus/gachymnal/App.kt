package com.gacpedromediateam.primus.gachymnal

import com.gacpedromediateam.primus.gachymnal.di.DaggerAppComponent
import com.gacpedromediateam.primus.gachymnal.utils.TimberTree
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import dagger.android.HasBroadcastReceiverInjector
import timber.log.Timber
import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric



class App : DaggerApplication(), HasBroadcastReceiverInjector {

    lateinit var injector: AndroidInjector<out DaggerApplication>

    private var androidDefaultUEH: Thread.UncaughtExceptionHandler? = null

    private val handler = Thread.UncaughtExceptionHandler { thread, ex ->
        androidDefaultUEH!!.uncaughtException(thread, ex)
        Timber.e(ex)
        Crashlytics.log(ex.toString())
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return injector
    }

    override fun onCreate() {
        injector = DaggerAppComponent.builder().application(this).build()
        super.onCreate()

        Fabric.with(this, Crashlytics())
        if (BuildConfig.DEBUG) {
            Timber.plant(TimberTree())
        }

        androidDefaultUEH = Thread.getDefaultUncaughtExceptionHandler()
        Thread.setDefaultUncaughtExceptionHandler(handler)
    }
}