package com.gacpedromediateam.primus.gachymnal.data.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull

/**
 * Created by Primus on 7/10/2017.
 */

@Entity(tableName = "main_hymn")
class MainHymn() {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    var id : Long? = null
    @NonNull
    var hymn_id: Int? = null
    var english: String? = null
    var yoruba: String? = null
    @NonNull

    var favorite: Int? = null

    @Ignore
    var title: String? = null



    @Ignore
    constructor(hymn_id: Int, english: String?, yoruba: String?, fave: Int?) :this() {
        this.hymn_id = hymn_id
        this.english = english
        this.yoruba = yoruba
        this.favorite = fave
    }



    constructor(id: Long, hymn_id: Int?, english: String?, yoruba: String?, fave: Int?) :this() {
        this.id = id
        this.hymn_id = hymn_id
        this.english = english
        this.yoruba = yoruba
        this.favorite = fave
    }

    fun convertHymn(hymn: Hymn) : MainHymn{
        this.id = hymn.id
        this.hymn_id = hymn.hymn_id
        this.english = hymn.english
        this.yoruba = hymn.yoruba
        this.favorite = hymn.favorite
        return this
    }

    override fun toString(): String {
        return "MainHymn{" +
                "hymn_id=" + hymn_id +
                ", english='" + english + '\''.toString() +
                ", favorite=" + favorite +
                ", yoruba='" + yoruba + '\''.toString() +
                ", title='" + title + '\''.toString() +
                '}'.toString()
    }
}

@Entity(tableName = "appendix_hymn")
class AppendixHymn(){

    @PrimaryKey(autoGenerate = true)
    @NonNull
    var id : Long? = null
    @NonNull
    var hymn_id: Int? = null
    var english: String? = null
    var yoruba: String? = null
    @NonNull
    var favorite: Int? = null

    @Ignore
    var title: String? = null



    @Ignore
    constructor(hymn_id: Int?, english: String?, yoruba: String?, fave: Int?) :this() {
        this.hymn_id = hymn_id
        this.english = english
        this.yoruba = yoruba
        this.favorite = fave
    }



    constructor(id: Long?, hymn_id: Int?, english: String?, yoruba: String?, fave: Int?) :this() {
        this.id = id
        this.hymn_id = hymn_id
        this.english = english
        this.yoruba = yoruba
        this.favorite = fave
    }

    fun convertHymn(hymn: Hymn) : AppendixHymn{
        this.id = hymn.id
        this.hymn_id = hymn.hymn_id
        this.english = hymn.english
        this.yoruba = hymn.yoruba
        this.favorite = hymn.favorite as Int
        return this
    }



    override fun toString(): String {
        return "AppHymn{" +
                "hymn_id=" + hymn_id +
                ", english='" + english + '\''.toString() +
                ", favorite=" + favorite +
                ", yoruba='" + yoruba + '\''.toString() +
                ", title='" + title + '\''.toString() +
                '}'.toString()
    }
}

class Hymn() {

    var id : Long? = null
    var hymn_id: Int? = null
    var english: String? = null
    var yoruba: String? = null
    var favorite: Int? = null

    @Ignore
    var title: String? = null



    @Ignore
    constructor(hymn_id: Int?, english: String?, yoruba: String?, fave: Int?) :this() {
        this.hymn_id = hymn_id
        this.english = english
        this.yoruba = yoruba
        this.favorite = fave
    }



    constructor(id: Long?, hymn_id: Int?, english: String?, yoruba: String?, fave: Int?) :this() {
        this.id = id
        this.hymn_id = hymn_id
        this.english = english
        this.yoruba = yoruba
        this.favorite = fave
    }


    override fun toString(): String {
        return "MainHymn{" +
                "hymn_id=" + hymn_id +
                ", english='" + english + '\''.toString() +
                ", favorite=" + favorite +
                ", yoruba='" + yoruba + '\''.toString() +
                ", title='" + title + '\''.toString() +
                '}'.toString()
    }
}

