package com.gacpedromediateam.primus.gachymnal.data.source.remote

import com.gacpedromediateam.primus.gachymnal.data.model.ServerResponse
import io.reactivex.Observable


import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

/**
 * Created by LordPrimus on 10/3/2017.
 */


interface Api {

    @FormUrlEncoded
    @POST("user/installations")
    fun postInstallations(@Field("UUID") UUID: String, @Field("phoneType") PhoneType: String, @Field("androidID") AndroidID: String): Observable<String>

    @FormUrlEncoded
    @POST("user/emails")
    fun sendEmail(@Field("email") Email: String): Observable<ServerResponse>

    companion object {

        //val EndPoint = "http://gacserver.000webhostapp.com/API/";
        val EndPoint = "http://10.0.2.2:8000/"
    }

}
