package com.gacpedromediateam.primus.gachymnal.ui.favorites

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.view.Menu
import android.view.MenuItem
import com.gacpedromediateam.primus.gachymnal.ui.contributors.ContributorActivity
import com.gacpedromediateam.primus.gachymnal.ui.hymn.Fragments.AppFragment
import com.gacpedromediateam.primus.gachymnal.ui.hymn.Fragments.FeedbackFragment
import com.gacpedromediateam.primus.gachymnal.ui.hymn.Fragments.MainFragment
import com.gacpedromediateam.primus.gachymnal.data.source.local.AppPreference
import com.gacpedromediateam.primus.gachymnal.utils.NetworkHelper
import com.gacpedromediateam.primus.gachymnal.R
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_favorites.*
import java.util.*
import javax.inject.Inject

class FavoritesActivity : DaggerAppCompatActivity() {

    private val mainFragment = MainFragment("favorite")
    private val appFragment = AppFragment("favorite")
    private val feedbackFragment = FeedbackFragment()
    @Inject
    lateinit var appPreference: AppPreference
    var prevMenuItem : MenuItem? = null
    internal var nh = NetworkHelper(this)
    val TAG = "HYMNACTIVITY"
    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                viewPager.currentItem = 0
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_dashboard -> {
                viewPager.currentItem = 1
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_notifications -> {
                viewPager.currentItem = 2
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(appPreference.isNightMode()){
            setTheme(R.style.AppThemeNight)
        }
        setContentView(R.layout.activity_favorites)
        setSupportActionBar(toolbar)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = ""
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        setupViewPager(viewPager)
        toolbar_title.text = "Likes"
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                if (prevMenuItem != null) {
                    prevMenuItem!!.isChecked = false
                } else {
                    navigation.menu.getItem(0).isChecked = false
                }

                navigation.menu.getItem(position).isChecked = true
                prevMenuItem = navigation.menu.getItem(position)
            }

            override fun onPageScrollStateChanged(state: Int) {

            }
        })

    }

    private class Adapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {
        private val mFragmentList = ArrayList<Fragment>()

        override fun getItem(position: Int): Fragment {
            return mFragmentList[position]
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFragment(fragment: Fragment) {
            mFragmentList.add(fragment)
        }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    @SuppressLint("NewApi")
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_fave -> {
                startActivity(Intent(this, FavoritesActivity::class.java))
                overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out)
                return true
            }
            R.id.action_contributors -> {
                startActivity(Intent(this, ContributorActivity::class.java))
                overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out)
                //Log.e(TAG, "onOptionsItemSelected: Contributors Clicked")
                return true
            }
            R.id.action_night_mode ->{
                if(appPreference.isNightMode()){
                    appPreference.setNightMode(false)
                }else{
                    appPreference.setNightMode(true)
                }
                startActivity(intent)
                finish()
            }

            else -> return super.onOptionsItemSelected(item)
        }
        return true
    }

    private fun setupViewPager(viewPager: ViewPager) {
        val adapter = Adapter(supportFragmentManager)
        adapter.addFragment(mainFragment)
        adapter.addFragment(appFragment)
        adapter.addFragment(feedbackFragment)
        viewPager.offscreenPageLimit = 3
        viewPager.adapter = adapter
    }

}
