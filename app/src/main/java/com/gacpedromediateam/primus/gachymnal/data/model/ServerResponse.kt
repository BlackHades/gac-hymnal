package com.gacpedromediateam.primus.gachymnal.data.model

/**
 * Created by mavericks on 5/1/18.
 */
class ServerResponse{
    var status : Int? = null
    var msg : String? = null
    override fun toString(): String {
        return "ServerResponse(status=$status, msg=$msg)"
    }

}