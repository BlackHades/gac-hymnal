package com.gacpedromediateam.primus.gachymnal.data.model

import com.google.gson.annotations.SerializedName

class DeviceData{
    @SerializedName("uuid")
    var uuid: String
    @SerializedName("phoneType")
    var phoneType: String
    @SerializedName("deviceId")
    var deviceId: String

    constructor(uuid: String, phoneType: String, deviceId: String){
        this.uuid = uuid
        this.phoneType = phoneType
        this.deviceId = deviceId
    }

    override fun toString(): String {
        return "DeviceData(uuid='$uuid', phoneType='$phoneType', deviceId='$deviceId')"
    }


}