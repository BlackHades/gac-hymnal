package com.gacpedromediateam.primus.gachymnal.ui.view

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.view.menu.MenuBuilder
import android.support.v7.view.menu.MenuPopupHelper
import android.support.v7.widget.PopupMenu
import android.util.Log
import android.view.*
import android.widget.*
import com.gacpedromediateam.primus.gachymnal.ui.contributors.ContributorActivity
import com.gacpedromediateam.primus.gachymnal.ui.favorites.FavoritesActivity
import com.gacpedromediateam.primus.gachymnal.data.source.local.AppPreference
import com.gacpedromediateam.primus.gachymnal.data.model.Hymn
import com.gacpedromediateam.primus.gachymnal.data.model.Verse
import com.gacpedromediateam.primus.gachymnal.R
import com.gacpedromediateam.primus.gachymnal.ui.hymn.HymnViewModel
import com.google.gson.Gson
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_view.*
import pl.polidea.view.ZoomView
import javax.inject.Inject
import kotlin.collections.ArrayList

class ViewActivity : DaggerAppCompatActivity() {
    var hymnType: String? = null
    var language: Int? = null
    lateinit var zoomView: ZoomView
    @Inject
    internal lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var appPreference: AppPreference
    private lateinit var mViewModel: HymnViewModel
    lateinit var mInflater: LayoutInflater
    internal var TAG = "ViewActivity"
    lateinit var payload: Hymn
    var verses = ArrayList<Verse>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(appPreference.isNightMode()){
            setTheme(R.style.AppThemeNight)
        }
        setContentView(R.layout.activity_view)
        mViewModel = ViewModelProviders.of(this, viewModelFactory).get(HymnViewModel::class.java)

        setSupportActionBar(toolbar)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = ""
        mInflater = LayoutInflater.from(this)
        val v = (getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(R.layout.view_hymn_layout, null, false)
        v.layoutParams = RelativeLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        zoomView = ZoomView(this)
        zoomView.addView(v)
        relativezoomId.addView(zoomView)
        val extras:Bundle = intent.extras
        if (extras.containsKey("title") && extras.containsKey("type") && extras.containsKey("id")) {
            hymnType = extras.getString("type")
            payload = Gson().fromJson(extras.getString("hymn"), Hymn::class.java)
        } else {
            Toast.makeText(this, "Error receiving data, Please restart app", Toast.LENGTH_SHORT).show()
        }
        populateList()
    }

    private fun populateList() {

        language = appPreference.getLanguage()
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            if (hymnType.equals("0"))
                toolbar_title.text = (if (language == 0) "Iwe Orin " else "Hymn ") + payload.hymn_id
            if (hymnType.equals("1"))
                toolbar_title.text = (if (language == 0) "Akokun " else "Appendix ") + payload.hymn_id

        }
        ////Log.e(TAG, "populateList: "+ payload);
        (findViewById<View>(R.id.viewHymnTitle) as TextView).text = if (language == 0) payload.yoruba else payload.english
//        initializeListView()
        getVerse(payload.hymn_id)
    }

    private fun initializeListView() {
        val listView = findViewById<ListView>(R.id.view_hymn_list)
        listView.adapter = HymnViewAdapter(this, verses, language!!)
        val v = mInflater.inflate(R.layout.amen, null)
        (v.findViewById<View>(R.id.AmenAmin) as TextView).text = if (language == 0) "Amin." else "Amen."
        Log.e(TAG, listView.footerViewsCount.toString())
        if (listView.footerViewsCount == 0) {
            listView.addFooterView(v)
        }
    }


    private fun getVerse(id: Int?) {
        mViewModel.getVerse(id!!, hymnType!!).observe(this, Observer { verses ->
            this.verses = verses!!
            initializeListView()
        })
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    @SuppressLint("RestrictedApi")
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_fave -> {
                startActivity(Intent(this, FavoritesActivity::class.java))
                overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out)
                return true
            }
            R.id.action_contributors -> {
                startActivity(Intent(this, ContributorActivity::class.java))
                overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out)
                //Log.e(TAG, "onOptionsItemSelected: Contributors Clicked")
                return true
            }
            R.id.action_language -> {
                val v : View = findViewById(R.id.toolbar)
                val popup = PopupMenu(this, v, Gravity.END)
                popup.menuInflater.inflate(R.menu.lang_menu, popup.menu)
                popup.setOnMenuItemClickListener {
                    //var language = 1
                    when(it.itemId){
                        R.id.action_english -> {
                            language = 1
                        }

                        R.id.action_yoruba -> {
                            language = 0
                        }

                    }
                    appPreference!!.setLanguage(language!!)
                    populateList()
                    return@setOnMenuItemClickListener true
                }
                val menuHelper = MenuPopupHelper(this, popup.menu as MenuBuilder, v)
                menuHelper.setForceShowIcon(true)
                menuHelper.show()
            }
            R.id.action_night_mode ->{
                if(appPreference!!.isNightMode()){
                    appPreference!!.setNightMode(false)
                }else{
                    appPreference!!.setNightMode(true)
                }
                startActivity(intent)
                finish()
            }
            else -> return super.onOptionsItemSelected(item)
        }
        return true
    }
}
