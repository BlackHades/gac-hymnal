package com.gacpedromediateam.primus.gachymnal.di

import android.app.Application
import com.gacpedromediateam.primus.gachymnal.App
import com.gacpedromediateam.primus.gachymnal.di.modules.*
import dagger.BindsInstance
import dagger.Component
import dagger.Module
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton
import kotlin.reflect.KClass


@Singleton
@Component(modules = [AndroidSupportInjectionModule::class,ActivityBindingModule::class, ViewModelModule::class,NetworkModule::class, AppModule::class, RoomModule::class])
interface AppComponent : AndroidInjector<App>{

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): AppComponent.Builder

        fun build(): AppComponent
    }
}