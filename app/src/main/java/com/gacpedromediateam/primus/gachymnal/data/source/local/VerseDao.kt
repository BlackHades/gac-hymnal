package com.gacpedromediateam.primus.gachymnal.data.source.local

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import com.gacpedromediateam.primus.gachymnal.data.model.Verse

@Dao
interface VerseDao {
    @Query("select * from main_verse where hymn_id = :id ORDER BY verse_id ASC")
    fun getMainVerse(id : Long) : LiveData<List<Verse>>

    @Query("SELECT * FROM appendix_verse where hymn_id == :id ORDER BY verse_id ASC")
    fun getAppendixVerse(id : Long) : LiveData<List<Verse>>
}