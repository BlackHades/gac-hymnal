package com.gacpedromediateam.primus.gachymnal.ui.splash

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import com.gacpedromediateam.primus.gachymnal.data.model.DeviceData
import com.gacpedromediateam.primus.gachymnal.data.repository.HymnalRepository
import com.gacpedromediateam.primus.gachymnal.data.source.local.AppPreference
import com.gacpedromediateam.primus.gachymnal.utils.AppSchedulers
import com.gacpedromediateam.primus.gachymnal.utils.Constants
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber
import javax.inject.Inject

class InstallationViewModel
@Inject
internal constructor (
        application: Application,
        var hymnalRepository: HymnalRepository,
        var appSchedulers: AppSchedulers,
        var appPreference: AppPreference) : AndroidViewModel(application){
    private var disposable: CompositeDisposable = CompositeDisposable()
    fun postEmail(email: String){
        disposable.add(hymnalRepository.postEmail(email)
                .subscribeOn(appSchedulers.io())
                .observeOn(appSchedulers.main())
                .subscribe(
                        {
                            res -> Timber.e(res.toString())
                            if(res.status == Constants.STATUS_SUCCESS)
                                appPreference.setSentMail(true)
                        },
                        {
                    err -> Timber.e(err)
                            appPreference.setSentMail(false)
                        }
                ))


    }

    fun postDeviceData(deviceData: DeviceData){
        disposable.add(hymnalRepository.sendInstallationData(deviceData)
                .subscribeOn(appSchedulers.io())
                .observeOn(appSchedulers.main())
                .subscribe(
                        {
                            res -> Timber.e(res.toString())
                            if(res.status == Constants.STATUS_SUCCESS)
                                appPreference.setSentDetails(true)
                        },
                        {
                            err -> Timber.e(err)
                            appPreference.setSentDetails(false)
                        }
                ))
    }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }

}
