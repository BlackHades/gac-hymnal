package com.gacpedromediateam.primus.gachymnal.data.source.local

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import android.arch.persistence.room.Update
import com.gacpedromediateam.primus.gachymnal.data.model.AppendixHymn
import com.gacpedromediateam.primus.gachymnal.data.model.Hymn


import com.gacpedromediateam.primus.gachymnal.data.model.MainHymn

@Dao
interface HymnDao {
    @get:Query("select * from main_hymn")
    val mainHymn: LiveData<List<Hymn>>

    @Query("select * from appendix_hymn")
    fun getAppendixHymn() : LiveData<List<Hymn>>

    @get:Query("select * from main_hymn where favorite=1")
    val mainFavoriteHymn: LiveData<List<Hymn>>

    @get:Query("select * from appendix_hymn where favorite=1")
    val appendixFavoriteHymn: LiveData<List<Hymn>>

    @Update(onConflict = REPLACE)
    fun updateMainHymn (mainHymn: MainHymn): Int

    @Update(onConflict = REPLACE)
    fun updateAppendixHymn (appendixHymn: AppendixHymn) :Int

    @Query("UPDATE main_hymn set favorite=:value where id=:id")
    fun updateMainHymn (id: Long, value: Int): Int
}
