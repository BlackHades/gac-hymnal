package com.gacpedromediateam.primus.gachymnal.di.modules

import android.app.Application
import android.arch.persistence.room.Room
import android.content.Context
import com.gacpedromediateam.primus.gachymnal.data.source.local.AppDatabase
import com.gacpedromediateam.primus.gachymnal.data.source.local.AppPreference
import com.gacpedromediateam.primus.gachymnal.data.source.local.HymnDao
import com.gacpedromediateam.primus.gachymnal.data.source.local.VerseDao
import com.google.gson.Gson
import com.huma.room_for_asset.RoomAsset
import dagger.Module
import dagger.Provides
import dagger.Reusable
import javax.inject.Singleton

@Module
class RoomModule {
    @Provides
    @Singleton
    internal fun provideAppDatabase(context: Application): AppDatabase {
        return RoomAsset.databaseBuilder(context, AppDatabase::class.java, AppDatabase.DATABASE_NAME)
                .allowMainThreadQueries()
                 .build()
    }

    @Provides
    @Singleton
    internal fun provideHymnDao(appDatabase: AppDatabase): HymnDao {
        return appDatabase.hymnDao()
    }

    @Provides
    @Singleton
    internal fun provideVerseDao(appDatabase: AppDatabase): VerseDao {
        return appDatabase.verseDao()
    }

    @Provides
    @Singleton
    internal fun provideAppPreference(context: Context, gson: Gson): AppPreference {
        return AppPreference(context, gson)
    }
}