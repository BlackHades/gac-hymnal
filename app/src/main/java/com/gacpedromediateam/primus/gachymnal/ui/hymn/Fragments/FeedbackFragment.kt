package com.gacpedromediateam.primus.gachymnal.ui.hymn.Fragments


import android.annotation.SuppressLint
import android.os.Bundle
import android.support.design.widget.CoordinatorLayout
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Button
import android.widget.LinearLayout
import android.widget.ProgressBar
import com.gacpedromediateam.primus.gachymnal.utils.NetworkHelper
import com.gacpedromediateam.primus.gachymnal.utils.Utility
import com.gacpedromediateam.primus.gachymnal.R
import com.gacpedromediateam.primus.gachymnal.data.source.local.AppPreference
import dagger.android.support.DaggerFragment
import javax.inject.Inject


/**
 * A simple [Fragment] subclass.
 */
class FeedbackFragment : DaggerFragment() {
    var main_progBar : ProgressBar? = null
    var webView : WebView? = null
    var TAG = "MainLayout"
    var cood: CoordinatorLayout? = null
    var currentUrl: String? = null
    var el: LinearLayout? = null
    var wl: LinearLayout? = null
    var rBtn: Button? = null
    //var appPreference: AppPreference? = null
    var base_url = "http://www.gridnigeria.com/"
    var error = false
    var myView : View? = null
    var webViewBundle : Bundle? = null
    @Inject
    lateinit var appPreference: AppPreference

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        myView = inflater.inflate(R.layout.fragment_feedback, container, false)
        main_progBar = myView!!.findViewById(R.id.main_progBar)
        wl = myView!!.findViewById(R.id.webLayout)
        el = myView!!.findViewById(R.id.error_layout)
        base_url = "http://www.gacpedro.com.ng/review/" + Utility(context!!, appPreference).androidID()

        el?.visibility = View.GONE
        cood = myView!!.findViewById(R.id.coodd)
        rBtn = myView!!.findViewById(R.id.refresh_btn)
        webView = myView!!.findViewById(R.id.feedbackWebview)
        currentUrl = base_url
        rBtn!!.setOnClickListener { loadView(currentUrl!!) }
        if (main_progBar != null) {
            main_progBar!!.visibility = View.VISIBLE
            main_progBar!!.isIndeterminate = true
            main_progBar!!.indeterminateDrawable.setColorFilter(resources.getColor(R.color.colorPrimary), android.graphics.PorterDuff.Mode.MULTIPLY)
        }
        loadView()
        return myView!!
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun loadView() {
        error = false
        val nh = NetworkHelper(context!!)
        if (nh.isConnected) {
            val webSettings = webView!!.settings
            webSettings.javaScriptEnabled = true
            wl!!.visibility = View.INVISIBLE
            webView!!.visibility = View.INVISIBLE
            main_progBar!!.visibility = View.VISIBLE
            webView!!.webViewClient = object : WebViewClient() {
                override fun onPageFinished(view: WebView, url: String) {
                    super.onPageFinished(view, url)
                    try{
                        if (!error) {
                            el!!.visibility = View.GONE
                            wl!!.visibility = View.VISIBLE
                            webView!!.visibility = View.VISIBLE
                        }
                        cood!!.setBackgroundColor(resources.getColor(R.color.white))
                    }catch (ex : Exception){
                        //Log.e(TAG, ex.toString())
                    }
                    //getSupportActionBar().setTitle(view.title)
                }

                override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                    currentUrl = url
                    loadView(base_url)
                    return true
                }

                override fun onReceivedError(view: WebView, errorCode: Int, description: String, failingUrl: String) {
                    //Log.e(TAG, "onReceivedError: $description - $failingUrl")
                    cood!!.setBackgroundColor(resources.getColor(R.color.white))
                    webView!!.visibility = View.INVISIBLE
                    error = true
                    currentUrl = failingUrl
                    el!!.visibility = View.VISIBLE
                    onAppError()
                }
            }
            if (webViewBundle == null) {
//                webView!!.loadUrl(base_url)
            } else {
                webView!!.restoreState(webViewBundle)
            }
        } else {
            onAppError()
        }

    }

    @SuppressLint("SetJavaScriptEnabled")
    fun loadView(url: String) {
        error = false
        val nh = NetworkHelper(context!!)
        if (nh.isConnected) {
            webView = myView!!.findViewById(R.id.feedbackWebview)
            val webSettings = webView!!.settings
            wl!!.visibility = View.INVISIBLE
            webView!!.visibility = View.INVISIBLE
            main_progBar!!.visibility = View.VISIBLE
            webSettings.javaScriptEnabled = true
            webView!!.webViewClient = object : WebViewClient() {
                override fun onPageFinished(view: WebView, url: String) {
                    super.onPageFinished(view, url)
                    if (!error) {
                        el!!.visibility = View.GONE
                        wl!!.visibility = View.VISIBLE
                        webView!!.visibility = View.VISIBLE
                        main_progBar!!.visibility = View.GONE
                    }
                    cood!!.setBackgroundColor(resources.getColor(R.color.white))
                    //getSupportActionBar().setTitle(view.title)
                }

                override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                    currentUrl = url
                    loadView(url)
                    return true
                }

                override fun onReceivedError(view: WebView, errorCode: Int, description: String, failingUrl: String) {
                    error = true
                    //Log.e(TAG, "onReceivedError: $description - $failingUrl")
                    cood!!.setBackgroundColor(resources.getColor(R.color.white))
                    webView!!.visibility = View.INVISIBLE
                    currentUrl = failingUrl
                    el!!.visibility = View.VISIBLE
                    onAppError()
                }
            }
            if (webViewBundle == null) {
                webView!!.loadUrl(url)
            } else {
                webView!!.restoreState(webViewBundle)
            }
        } else {
            onAppError()
        }

    }

    fun onAppError() {
        cood!!.setBackgroundColor(resources.getColor(R.color.white))
        wl!!.visibility = View.VISIBLE
        el!!.visibility = View.VISIBLE
        main_progBar!!.visibility = View.GONE
    }

}// Required empty public constructor
