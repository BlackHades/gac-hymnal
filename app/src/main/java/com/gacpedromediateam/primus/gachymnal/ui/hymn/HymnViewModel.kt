package com.gacpedromediateam.primus.gachymnal.ui.hymn

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.gacpedromediateam.primus.gachymnal.data.model.AppendixHymn
import com.gacpedromediateam.primus.gachymnal.data.model.Hymn
import com.gacpedromediateam.primus.gachymnal.data.model.MainHymn
import com.gacpedromediateam.primus.gachymnal.data.model.Verse
import com.gacpedromediateam.primus.gachymnal.data.repository.HymnalRepository
import com.gacpedromediateam.primus.gachymnal.utils.AppSchedulers
import com.gacpedromediateam.primus.gachymnal.utils.Constants
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber
import javax.inject.Inject

class HymnViewModel
@Inject
internal constructor(
        application: Application,
        private val hymnalRepository: HymnalRepository): AndroidViewModel(application) {

    fun getLocalMainHymn(type: String?):LiveData<ArrayList<Hymn>>{
        return if(type == null)
            hymnalRepository.getMainHymn()
        else
            hymnalRepository.getMainFavorite()
    }

    fun getLocalAppendixHymn(type: String?):LiveData<ArrayList<Hymn>>{
        return if(type == null)
            hymnalRepository.getAppendixHymn()
        else
            hymnalRepository.getAppendixFavorite()
    }

    fun updateMainHymn(hymn: Hymn){
        Timber.e(hymnalRepository.updateMainFavorite(hymn).toString())
    }


    fun getVerse(id: Int, type: String):LiveData<ArrayList<Verse>> {
        return if(type == "1") hymnalRepository.getAppenidixVerse(id)
        else hymnalRepository.getMainVerse(id)
    }

}