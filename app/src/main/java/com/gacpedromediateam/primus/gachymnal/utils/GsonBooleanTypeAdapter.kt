package com.gacpedromediateam.primus.gachymnal.utils

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonParseException

import java.lang.reflect.Type

class GsonBooleanTypeAdapter : JsonDeserializer<Boolean> {
    @Throws(JsonParseException::class)
    override fun deserialize(json: JsonElement, typeOfT: Type, context: JsonDeserializationContext): Boolean? {
        try {
            val code = json.asInt
            return code == 1
        } catch (ex: Exception) {
            return java.lang.Boolean.valueOf(json.toString())
        }

    }


}
