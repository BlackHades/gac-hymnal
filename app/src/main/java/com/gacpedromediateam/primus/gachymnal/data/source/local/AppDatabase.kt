package com.gacpedromediateam.primus.gachymnal.data.source.local

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.gacpedromediateam.primus.gachymnal.data.model.*

@Database(entities = [MainHymn::class, AppendixHymn::class, MainVerse::class, AppendixVerse::class], version = 2,  exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun hymnDao(): HymnDao
    abstract fun verseDao(): VerseDao

    companion object {
        const val DATABASE_NAME = "gac.db"
    }
}
