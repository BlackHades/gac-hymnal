package com.gacpedromediateam.primus.gachymnal.utils

import android.content.Context
import android.os.Build
import android.provider.Settings
import com.gacpedromediateam.primus.gachymnal.data.source.local.AppPreference
import java.util.*

/**
 * Created by Primus on 7/9/2017.
 */

class Utility(var context: Context, val appPreference: AppPreference) {

    fun androidUUID(): String {
        val uniqueID = UUID.randomUUID().toString()
        val uid = appPreference.getUUID()
        uid?.let {
            return it
        }
        appPreference.saveUUID(uniqueID)
        return uniqueID
    }

    fun androidID(): String {
        return Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
    }

    fun phoneType(): String {
        return Build.BRAND + " " + Build.MODEL + " " + Build.ID + " " + Build.DEVICE + " " + Build.PRODUCT
    }

}

