package com.gacpedromediateam.primus.gachymnal.ui.hymn

import com.gacpedromediateam.primus.gachymnal.di.annotations.FragmentScoped
import com.gacpedromediateam.primus.gachymnal.ui.hymn.Fragments.AppFragment
import com.gacpedromediateam.primus.gachymnal.ui.hymn.Fragments.FeedbackFragment
import com.gacpedromediateam.primus.gachymnal.ui.hymn.Fragments.MainFragment

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class HymnActivityModule {
    @FragmentScoped
    @ContributesAndroidInjector
    abstract fun mainFragment(): MainFragment

    @FragmentScoped
    @ContributesAndroidInjector
    abstract fun appFragment(): AppFragment

    @FragmentScoped
    @ContributesAndroidInjector
    abstract fun feedbackFragment(): FeedbackFragment
}
