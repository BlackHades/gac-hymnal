package com.gacpedromediateam.primus.gachymnal.ui.splash

import android.accounts.AccountManager
import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomSheetDialog
import android.support.v7.app.AlertDialog
import android.view.ContextThemeWrapper
import android.view.LayoutInflater
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import com.gacpedromediateam.primus.gachymnal.data.source.local.AppPreference
import com.gacpedromediateam.primus.gachymnal.utils.NetworkHelper
import com.gacpedromediateam.primus.gachymnal.utils.Utility
import com.gacpedromediateam.primus.gachymnal.R
import com.google.firebase.analytics.FirebaseAnalytics
import dagger.android.support.DaggerAppCompatActivity
import java.util.*
import java.util.concurrent.TimeUnit
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Handler
import com.crashlytics.android.Crashlytics
import com.gacpedromediateam.primus.gachymnal.data.model.DeviceData
import com.gacpedromediateam.primus.gachymnal.ui.hymn.HymnActivity
import com.gacpedromediateam.primus.gachymnal.utils.Constants
import timber.log.Timber
import javax.inject.Inject




class SplashActivity : DaggerAppCompatActivity() {

    private var nh: NetworkHelper? = null
    private lateinit var util: Utility
    private var mFirebaseAnalytics: FirebaseAnalytics? = null
    private var env = "live"

    @Inject
    internal lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var appPreference: AppPreference
    private lateinit var mViewModel: InstallationViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(appPreference.isNightMode()){
            setTheme(R.style.AppThemeNight)
        }
        setContentView(R.layout.activity_splash)
        mViewModel = ViewModelProviders.of(this, viewModelFactory).get(InstallationViewModel::class.java)

        nh = NetworkHelper(this)
        util = Utility(this, appPreference)
        try {
            startUp()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }

    }


    private fun selectLanguage() {
        runOnUiThread {
            val mBottomSheet = BottomSheetDialog(this@SplashActivity)
            val sheetView = layoutInflater.inflate(R.layout.user_lang_select, null)
            mBottomSheet.setContentView(sheetView)
            val english = sheetView.findViewById<LinearLayout>(R.id.english_lang)
            val yoruba = sheetView.findViewById<LinearLayout>(R.id.yoruba_lang)
            english.setOnClickListener {
                appPreference.setLanguage(Constants.LANGUAGE_ENGLISH)
                appPreference.setFirstRun(false)
                mBottomSheet.dismiss()
                bringUpEmailDialog()
            }
            yoruba.setOnClickListener {
                appPreference.setLanguage(Constants.LANGUAGE_YORUBA)
                appPreference.setFirstRun(false)
                mBottomSheet.dismiss()
                bringUpEmailDialog()
            }
            mBottomSheet.setCanceledOnTouchOutside(false)
            mBottomSheet.setCancelable(false)
            mBottomSheet.show()
        }

    }


    private fun callPostApi() {
        mViewModel.postDeviceData(DeviceData(util.androidUUID(), util.phoneType(), util.androidID()))
    }
    private fun sendEmail(email : String?) {
        email?.let {
            mViewModel.postEmail(email)
        }

        goToHome()
    }


    private fun startUp(){
        Handler().postDelayed({
            if (appPreference.getFirstRun()) {
                Timber.e("First Run true")
                callPostApi()
                selectLanguage()
            } else {
                Timber.e("First Run false")

                if (env == "live") {
                    Timber.e("Live true")
//                        if (true) {
                    if (nh!!.isConnected) {
                        Timber.e("Connected true")

                        if (!appPreference.getSentDetails()){
                            Timber.e("Sent Details true")
                            callPostApi()
                        }

                        if(!appPreference.getSentMail()){
                            Timber.e("Sent Email true")
                            val email = appPreference.getEmail()
                            if(email == null) bringUpEmailDialog()
                            else{
                                sendEmail(email)
                            }
                        }else
                            goToHome()
                    }else
                        goToHome()
                }
                else
                    goToHome()
            }
        },TimeUnit.SECONDS.toMillis(0).toInt().toLong())
    }

    @SuppressLint("RestrictedApi")
    private fun bringUpEmailDialog() {
        var et = ""
        try {
            val account = AccountManager.get(this).getAccountsByType("com.google")
            account.let {
                et = account[0].name.toString()
            }
        } catch (ex: Exception) {
            //Log.e("ErrorEmail", ex.toString())
        }
        runOnUiThread {
           val dialog = AlertDialog.Builder(ContextThemeWrapper(this, R.style.AppThemeNight)).create()
            dialog.setCanceledOnTouchOutside(false)
           val v = LayoutInflater.from(this).inflate(R.layout.email_dialog,null)
           val email = v.findViewById<EditText>(R.id.user_email)
            email.setText(et)
           v.findViewById<Button>(R.id.email_submit).setOnClickListener {
               if(email.text.isEmpty() || !email.text.toString().contains("@")){
                   email.error = "Enter A Valid Email Address"
               }else{
                   dialog.dismiss()
                   appPreference.setEmail(email.text.toString())
                   sendEmail(email.text.toString())
                   goToHome()
               }
           }

           v.findViewById<Button>(R.id.email_cancel).setOnClickListener {
               dialog.dismiss()
               goToHome()
           }
           dialog.setView(v)
           dialog.show()
        }
    }

    private fun goToHome(){
        startActivity(Intent(this@SplashActivity, HymnActivity::class.java))
        finish()
    }
}
