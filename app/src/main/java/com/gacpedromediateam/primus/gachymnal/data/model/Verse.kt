package com.gacpedromediateam.primus.gachymnal.data.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull

/**
 * Created by Primus on 7/11/2017.
 */

class Verse() {
    @PrimaryKey var id: Long? = null
    var hymn_id: Int? = null
    var verse_id: Int? = null
    var english: String? = null
    var yoruba: String? = null
    @Ignore
    var word: String? = null


    @Ignore
    constructor(verse_id: Int?, word: String) : this() {
        this.verse_id = verse_id
        this.word = word
    }

    @Ignore
    constructor(hymn_id: Int?, verse_id: Int?, english: String, yoruba: String) : this() {
        this.hymn_id = hymn_id
        this.verse_id = verse_id
        this.english = english
        this.yoruba = yoruba
    }


    constructor(id : Long?, hymn_id: Int?, verse_id: Int?, english: String, yoruba: String) : this() {
        this.id = id
        this.hymn_id = hymn_id
        this.verse_id = verse_id
        this.english = english
        this.yoruba = yoruba
    }

    override fun toString(): String {
        return "Verse{" +
                "hymn_id=" + hymn_id +
                ", verse_id=" + verse_id +
                ", english='" + english + '\''.toString() +
                ", yoruba='" + yoruba + '\''.toString() +
                ", word='" + word + '\''.toString() +
                '}'.toString()
    }


}


@Entity(tableName = "main_verse")
class MainVerse() {
    @PrimaryKey @NonNull var id: Long? = null
    @NonNull var hymn_id: Int? = null
    @NonNull var verse_id: Int? = null
    var english: String? = null
    var yoruba: String? = null
    @Ignore var word: String? = null


    @Ignore
    constructor(verse_id: Int?, word: String) : this() {
        this.verse_id = verse_id
        this.word = word
    }

    @Ignore
    constructor(hymn_id: Int?, verse_id: Int?, english: String, yoruba: String) : this() {
        this.hymn_id = hymn_id
        this.verse_id = verse_id
        this.english = english
        this.yoruba = yoruba
    }


    constructor(id : Long?, hymn_id: Int?, verse_id: Int?, english: String, yoruba: String) : this() {
        this.id = id
        this.hymn_id = hymn_id
        this.verse_id = verse_id
        this.english = english
        this.yoruba = yoruba
    }

    override fun toString(): String {
        return "Verse{" +
                "hymn_id=" + hymn_id +
                ", verse_id=" + verse_id +
                ", english='" + english + '\''.toString() +
                ", yoruba='" + yoruba + '\''.toString() +
                ", word='" + word + '\''.toString() +
                '}'.toString()
    }


}



@Entity(tableName = "appendix_verse")
class AppendixVerse() {
    @PrimaryKey @NonNull var id: Long? = null
    @NonNull var hymn_id: Int? = null
    @NonNull var verse_id: Int? = null
    var english: String? = null
    var yoruba: String? = null
    @Ignore  var word: String? = null


    @Ignore
    constructor(verse_id: Int?, word: String) : this() {
        this.verse_id = verse_id
        this.word = word
    }

    @Ignore
    constructor(hymn_id: Int?, verse_id: Int?, english: String, yoruba: String) : this() {
        this.hymn_id = hymn_id
        this.verse_id = verse_id
        this.english = english
        this.yoruba = yoruba
    }


    constructor(id : Long?, hymn_id: Int?, verse_id: Int?, english: String, yoruba: String) : this() {
        this.id = id
        this.hymn_id = hymn_id
        this.verse_id = verse_id
        this.english = english
        this.yoruba = yoruba
    }

    override fun toString(): String {
        return "Verse{" +
                "hymn_id=" + hymn_id +
                ", verse_id=" + verse_id +
                ", english='" + english + '\''.toString() +
                ", yoruba='" + yoruba + '\''.toString() +
                ", word='" + word + '\''.toString() +
                '}'.toString()
    }


}

