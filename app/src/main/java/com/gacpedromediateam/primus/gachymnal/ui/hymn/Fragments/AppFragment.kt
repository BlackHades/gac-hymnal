package com.gacpedromediateam.primus.gachymnal.ui.hymn.Fragments


import android.annotation.SuppressLint
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.view.menu.MenuBuilder
import android.support.v7.view.menu.MenuPopupHelper
import android.support.v7.widget.PopupMenu
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ListView
import android.widget.SearchView
import com.gacpedromediateam.primus.gachymnal.ui.view.ViewActivity
import com.gacpedromediateam.primus.gachymnal.ui.hymn.HymnListAdapter
import com.gacpedromediateam.primus.gachymnal.data.source.local.AppPreference
import com.gacpedromediateam.primus.gachymnal.data.model.Hymn
import com.gacpedromediateam.primus.gachymnal.R
import com.google.gson.Gson
import com.gacpedromediateam.primus.gachymnal.ui.hymn.HymnViewModel
import dagger.android.support.DaggerFragment
import javax.inject.Inject


/**
 * A simple [Fragment] subclass.
 */
class AppFragment : DaggerFragment {

    private var language: Int? = null
    lateinit var adapter: HymnListAdapter
    var TAG = "App List"
//    lateinit var db: DbHelper? = null
    var type: String?
    lateinit var hymn : Hymn
    @Inject
    internal lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var appPreference: AppPreference
    private lateinit var mViewModel: HymnViewModel
    private var hymns: ArrayList<Hymn> = ArrayList()
    constructor() {
        this.type = null
    }

    @SuppressLint("ValidFragment")
    constructor(fave: String) {
        this.type = fave
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(appPreference.isNightMode()){
            context!!.setTheme(R.style.AppThemeNight)
        }
        mViewModel = ViewModelProviders.of(this, viewModelFactory).get(HymnViewModel::class.java)
    }


    @SuppressLint("RestrictedApi")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_app_hymn, container, false)
        this.language = appPreference.getLanguage()

        return view
    }


    override fun onResume() {
        super.onResume()
        initializeListView()
        observeAppendixHymn()
        if(appPreference.getLanguage() != language){
            language = appPreference.getLanguage()
            adapter.changeLanguage(language!!)
        }
    }

    @SuppressLint("RestrictedApi")
    private fun initializeListView() {
        val listView = view!!.findViewById<ListView>(R.id.appendix_list_view)
        adapter = HymnListAdapter(context, hymns, language)
        listView.adapter = adapter

        listView.onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ ->
            val fullObject = listView.getItemAtPosition(position) as Hymn
            startActivity(Intent(context!!, ViewActivity::class.java).apply {
                putExtra("hymn", Gson().toJson(fullObject))
                putExtra("id", fullObject.hymn_id)
                putExtra("title", fullObject.title)
                putExtra("type", 1.toString())
            })

            activity!!.overridePendingTransition(R.anim.fadein, R.anim.fadeout)
        }
        listView.onItemLongClickListener = AdapterView.OnItemLongClickListener { _, view, position, _ ->
            val o = listView.getItemAtPosition(position)
            hymn = o as Hymn
            val popup = PopupMenu(context!!, view, Gravity.END)
            popup.menuInflater.inflate(if (hymn.favorite!! == 1) R.menu.fave_no else R.menu.fave, popup.menu)
            popup.setOnMenuItemClickListener {
                if(it.itemId == R.id.fave)hymns[position].favorite = 1
                if(it.itemId == R.id.no_fave)hymns[position].favorite = 0
                if(this.type != null)hymns.removeAt(position)
                adapter.swap(hymns)
                return@setOnMenuItemClickListener true
            }
            val menuHelper = MenuPopupHelper(context!!, popup.menu as MenuBuilder, view)
            menuHelper.setForceShowIcon(true)
            menuHelper.gravity = Gravity.END
            menuHelper.show()
            true
        }
        val inputText = view!!.findViewById<SearchView>(R.id.search_view_app)
        inputText.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(s: String): Boolean {
                return false
            }

            override fun onQueryTextChange(s: String): Boolean {
                adapter.filter.filter(s)
                return false
            }
        })    }

    private fun observeAppendixHymn(){
        mViewModel.getLocalAppendixHymn(type).observe(this, android.arch.lifecycle.Observer { hymns ->
            if(hymns != null && hymns.size > 0){
                this.hymns = hymns
                adapter.swap(hymns)
            }
        })
    }
}
