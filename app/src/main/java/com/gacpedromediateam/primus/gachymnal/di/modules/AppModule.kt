package com.gacpedromediateam.primus.gachymnal.di.modules

import android.app.Application
import android.content.Context
import dagger.Binds
import dagger.Module

@Module
interface AppModule {
    @Binds
    abstract fun provideApplication(application: Application): Context

}