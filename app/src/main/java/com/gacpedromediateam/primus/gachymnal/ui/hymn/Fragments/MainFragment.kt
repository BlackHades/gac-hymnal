package com.gacpedromediateam.primus.gachymnal.ui.hymn.Fragments


import android.annotation.SuppressLint
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.view.menu.MenuBuilder
import android.support.v7.view.menu.MenuPopupHelper
import android.support.v7.widget.PopupMenu
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ListView
import android.widget.SearchView
import com.gacpedromediateam.primus.gachymnal.ui.view.ViewActivity
import com.gacpedromediateam.primus.gachymnal.ui.hymn.HymnListAdapter
import com.gacpedromediateam.primus.gachymnal.data.source.local.AppPreference
import com.gacpedromediateam.primus.gachymnal.R
import com.google.gson.Gson
import com.gacpedromediateam.primus.gachymnal.data.model.Hymn
import com.gacpedromediateam.primus.gachymnal.ui.hymn.HymnViewModel
import dagger.android.support.DaggerFragment
import timber.log.Timber
import javax.inject.Inject
import kotlin.collections.ArrayList

/**
 * A simple [Fragment] subclass.
 */
class MainFragment : DaggerFragment {
    private var language: Int? = null
    lateinit var adapter: HymnListAdapter
    internal val TAG = "MainFragment"
    var type: String?
    lateinit var hymn : Hymn
    @Inject
    internal lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var appPreference: AppPreference
    private lateinit var mViewModel: HymnViewModel
    private var hymns:ArrayList<Hymn> = ArrayList()
    constructor() {
        // Required empty public constructor
        this.type = null
    }

    @SuppressLint("ValidFragment")
    constructor(type: String) {
        this.type = type
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel = ViewModelProviders.of(this, viewModelFactory).get(HymnViewModel::class.java)
//        mViewModel.getLocalMainHymn()
    }


    @SuppressLint("RestrictedApi")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View? = inflater.inflate(R.layout.fragment_main_hymn, container, false)
        language = appPreference.getLanguage()
        return view
    }

    override fun onResume() {
        super.onResume()
        initializeListView()
        observeMainHymn()
         if(appPreference.getLanguage() != language){
            language = appPreference.getLanguage()
            adapter.changeLanguage(language!!)
        }
    }

    private fun initializeListView() {
        val listView = view!!.findViewById<ListView>(R.id.main_list_view)
        adapter = HymnListAdapter(this.activity, hymns, language)
        listView.adapter = adapter
        listView.onItemClickListener = AdapterView.OnItemClickListener { a, _, position, _ ->
            val fullObject = listView.getItemAtPosition(position) as Hymn
            startActivity(Intent(activity, ViewActivity::class.java).apply {
                putExtra("hymn", Gson().toJson(fullObject))
                putExtra("id", fullObject.hymn_id)
                putExtra("title", fullObject.title)
                putExtra("type", 0.toString())
            })

            activity!!.overridePendingTransition(R.anim.fadein, R.anim.fadeout)
        }
        listView.onItemLongClickListener = AdapterView.OnItemLongClickListener { _, view, position, _ ->
            val o = listView.getItemAtPosition(position)
            hymn = o as Hymn
            val popup = PopupMenu(activity!!, view, Gravity.END)
            popup.menuInflater.inflate(if (hymn.favorite!! == 1) R.menu.fave_no else R.menu.fave, popup.menu)
            popup.setOnMenuItemClickListener {
                Log.e(TAG, hymns[position].toString())
                if(it.itemId == R.id.fave)hymns[position].favorite = 1
                if(it.itemId == R.id.no_fave)hymns[position].favorite = 0
                Log.e(TAG, hymns[position].toString())
                mViewModel.updateMainHymn(hymns[position])
                // db!!.setMainFavorite(hymn!!.hymn_id!!, getHymns[position].favorite!!)
                if (type != null) {
                    hymns.removeAt(position)
                }
//                        adapter.notifyDataSetChanged()
                adapter.swap(hymns)
                return@setOnMenuItemClickListener true
            }
            val menuHelper = MenuPopupHelper(activity!!, popup.menu as MenuBuilder, view)
            menuHelper.setForceShowIcon(true)
            menuHelper.show()
            true

        }

        val inputText = view!!.findViewById<SearchView>(R.id.search_view)
        inputText.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(s: String): Boolean {
                return false
            }

            override fun onQueryTextChange(s: String): Boolean {
                Timber.e(s)
                adapter.filter.filter(s)
                return false
            }
        })
    }

    @SuppressLint("RestrictedApi")
    private fun observeMainHymn() {
        //Get Data using room
        mViewModel.getLocalMainHymn(type).observe(this, android.arch.lifecycle.Observer { hymns ->
//            Timber.e(hymns.toString())
            if(hymns != null || hymns!!.size > 0 ){
                if(this.hymns.size < hymns.size){
                    this.hymns = hymns
                    adapter.swap(this.hymns)
                }

            }

        })
    }
}
