package com.gacpedromediateam.primus.gachymnal.ui.contributors

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView

import com.gacpedromediateam.primus.gachymnal.data.model.Team
import com.gacpedromediateam.primus.gachymnal.R

/**
 * Created by micheal on 10/12/2017.
 */

class ContributorAdapter(private val context: Context, private val teams: List<Team>) : BaseAdapter() {
    private val mInflater: LayoutInflater = LayoutInflater.from(context)

    override fun getCount(): Int {
        return teams.size
    }

    override fun getItem(position: Int): Any {
        return teams[position]
    }

    override fun getItemId(position: Int): Long {
        return teams.size.toLong()
    }

    override fun getView(position: Int, altView: View?, parent: ViewGroup): View {
        var convertView = altView
        val holder: ViewHolder
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.contri_view, null)
            holder = ViewHolder()
            //holder.ID = convertView.findViewById(R.id.cId);
            holder.Name = convertView!!
                    .findViewById(R.id.cName)
            convertView.tag = holder
        } else {
            holder = convertView.tag as ViewHolder
        }

        //holder.ID.setText("");
        //holder.ID.setText(String.valueOf(teams.get(position).getId()));
        holder.Name!!.text = teams[position].name

        return convertView
    }

    internal class ViewHolder {
        var ID: TextView? = null
        var Name: TextView? = null
    }
}
