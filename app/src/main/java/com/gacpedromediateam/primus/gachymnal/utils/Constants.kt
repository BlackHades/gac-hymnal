package com.gacpedromediateam.primus.gachymnal.utils

object Constants {
    const val USER_QUALIFIER = "USER_QUALIFIER"
    const val STATUS_SUCCESS = 1

    const val LANGUAGE_ENGLISH = 1
    const val LANGUAGE_YORUBA = 0
//    var DEFAULT_HOST = "http://192.168.8.101:3000/api/"
    var DEFAULT_HOST = "https://enigmatic-refuge-51405.herokuapp.com/api/"
}
