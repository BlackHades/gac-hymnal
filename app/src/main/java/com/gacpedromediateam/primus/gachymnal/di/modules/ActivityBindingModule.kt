package com.gacpedromediateam.primus.gachymnal.di.modules

import com.gacpedromediateam.primus.gachymnal.ui.contributors.ContributorActivity
import com.gacpedromediateam.primus.gachymnal.ui.favorites.FavoritesActivity
import com.gacpedromediateam.primus.gachymnal.ui.hymn.HymnActivity
import com.gacpedromediateam.primus.gachymnal.ui.splash.SplashActivity
import com.gacpedromediateam.primus.gachymnal.ui.view.ViewActivity
import com.gacpedromediateam.primus.gachymnal.di.annotations.ActivityScoped
import com.gacpedromediateam.primus.gachymnal.ui.hymn.HymnActivityModule

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface ActivityBindingModule {
    @ActivityScoped
    @ContributesAndroidInjector(modules = [HymnActivityModule::class])
    fun hymnActivity(): HymnActivity


    @ActivityScoped
    @ContributesAndroidInjector(modules = [HymnActivityModule::class])
    fun favoritesActivity(): FavoritesActivity

    @ActivityScoped
    @ContributesAndroidInjector
    fun splashActivity(): SplashActivity

    @ActivityScoped
    @ContributesAndroidInjector
    fun viewActivity(): ViewActivity

    @ActivityScoped
    @ContributesAndroidInjector
    fun contributorActivity(): ContributorActivity
}
