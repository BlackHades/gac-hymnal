package com.gacpedromediateam.primus.gachymnal.data.source.remote

import com.gacpedromediateam.primus.gachymnal.data.model.DeviceData
import com.gacpedromediateam.primus.gachymnal.data.model.Response
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface HymnalApiService {
    @POST("installations/save")
    fun postInstallations(@Body deviceData: DeviceData): Observable<Response<String>>

    @FormUrlEncoded
    @POST("installations/email")
    fun sendEmail(@Field("email") Email: String): Observable<Response<String>>

}