package com.gacpedromediateam.primus.gachymnal.di.modules

import com.gacpedromediateam.primus.gachymnal.BuildConfig
import com.gacpedromediateam.primus.gachymnal.data.source.local.AppPreference
import com.gacpedromediateam.primus.gachymnal.data.source.remote.HymnalApiService
import com.gacpedromediateam.primus.gachymnal.di.annotations.NamedQualifier
import com.gacpedromediateam.primus.gachymnal.utils.Constants
import com.gacpedromediateam.primus.gachymnal.utils.GsonBooleanTypeAdapter
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.Reusable
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.util.concurrent.TimeUnit

@Module
object NetworkModule {



    @Provides
    @JvmStatic
    @Reusable
     internal fun provideHymnalApiService(@NamedQualifier(Constants.USER_QUALIFIER) okHttpClient: OkHttpClient,
                                         gson: Gson): HymnalApiService {
        val retrofitBuilder = Retrofit.Builder()
        retrofitBuilder.baseUrl(Constants.DEFAULT_HOST)

        retrofitBuilder.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        retrofitBuilder.client(okHttpClient)
        retrofitBuilder.addConverterFactory(GsonConverterFactory.create(gson))
        return retrofitBuilder.build().create(HymnalApiService::class.java)
    }

    @Provides
    @JvmStatic
    @Reusable
    @NamedQualifier(Constants.USER_QUALIFIER)
    internal fun provideUserOkhttpClient(@NamedQualifier(Constants.USER_QUALIFIER) interceptor: Interceptor, httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
        val builder = OkHttpClient.Builder()
        builder.connectTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(1, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES)
                .addInterceptor(interceptor)
                .addInterceptor(httpLoggingInterceptor)
        return builder.build()
    }


    @Provides
    @JvmStatic
    @Reusable
    @NamedQualifier(Constants.USER_QUALIFIER)
    internal fun provideUserInterceptor(appPreference: AppPreference): Interceptor {
//        return { chain ->
//
//            val original = chain.request()
//
//            val newRequest = original.newBuilder()
//                    .addHeader("Accept", "application/json")
//                    .addHeader("Content-Type", "application/json")
//                    .build()
//
//            chain.proceed(newRequest)
//        }

        val interceptor = HttpLoggingInterceptor()
        if (BuildConfig.DEBUG)
            interceptor.level = HttpLoggingInterceptor.Level.BODY
        else
            interceptor.level = HttpLoggingInterceptor.Level.NONE

        return interceptor
    }

    @Provides
    @JvmStatic
    @Reusable
    internal fun provideGson(): Gson {
        val gsonBuilder = GsonBuilder()
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        gsonBuilder.registerTypeAdapter(Boolean::class.javaPrimitiveType, GsonBooleanTypeAdapter())
        return gsonBuilder.create()
    }

    @Provides
    @JvmStatic
    @Reusable
    internal fun httpLoggingInterceptor(): HttpLoggingInterceptor {
        val httpLoggingInterceptor = HttpLoggingInterceptor { message -> Timber.d(message) }
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return httpLoggingInterceptor
    }
}