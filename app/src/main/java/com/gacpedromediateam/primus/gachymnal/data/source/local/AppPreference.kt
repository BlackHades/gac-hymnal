package com.gacpedromediateam.primus.gachymnal.data.source.local

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences

import com.google.gson.Gson
import javax.inject.Singleton
import javax.inject.Inject



/**
 * Created by micheal on 11/11/2017.
 */

@Singleton
class AppPreference @Inject internal constructor(var context: Context, var gson: Gson) {
    var spref: SharedPreferences = context.getSharedPreferences(context.packageName, MODE_PRIVATE)


    fun setFirstRun(value : Boolean){
        val editor = spref.edit()
        editor.putBoolean("atFirstRun", value).apply()
    }
    fun getFirstRun() : Boolean{
        return spref.getBoolean("atFirstRun", true)
    }

    fun getLanguage() : Int{
        return spref.getInt("language", 1)
    }

    fun setLanguage(value : Int){
        val editor = spref.edit()
        editor.putInt("language", value).apply()
    }

    fun getSentDetails() : Boolean{
        return spref.getBoolean("sentDetails", false)
    }

    fun setSentDetails(value : Boolean){
        val editor = spref.edit()
        editor.putBoolean("sentDetails", value).apply()
    }

    fun getEmail() : String?{
        return spref.getString("email",null)
    }

    fun setEmail(value:String){
        val editor = spref.edit()
        editor.putString("email",value).apply()
    }

    fun getSentMail() : Boolean{
        return spref.getBoolean("sent_mail",false)
    }

    fun setSentMail(value : Boolean){
        val editor = spref.edit()
        editor.putBoolean("sent_mail",value).apply()
    }
    fun isNightMode() : Boolean{
        return spref.getBoolean("night_mode",false)
    }
    fun setNightMode(value : Boolean){
        val editor = spref.edit()
        editor.putBoolean("night_mode",value).apply()
    }

    fun saveUUID(value: String) {
        val editor = spref.edit()
        editor.putString("uuid", value).apply()
    }

    fun getUUID(): String? {
        return spref.getString("uuid", null)
    }
}
