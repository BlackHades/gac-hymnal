package com.gacpedromediateam.primus.gachymnal.ui.contributors

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.view.menu.MenuBuilder
import android.support.v7.view.menu.MenuPopupHelper
import android.support.v7.widget.PopupMenu
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ListView
import com.gacpedromediateam.primus.gachymnal.data.source.local.AppPreference
import com.gacpedromediateam.primus.gachymnal.utils.NetworkHelper
import com.gacpedromediateam.primus.gachymnal.data.model.Team
import com.gacpedromediateam.primus.gachymnal.R
import com.gacpedromediateam.primus.gachymnal.ui.favorites.FavoritesActivity
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_contributor.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class ContributorActivity : DaggerAppCompatActivity() {

    var teams: ArrayList<Team> = ArrayList()
    internal var TAG = "Contributor"
    private var adapter: ContributorAdapter? = null
    var nh = NetworkHelper(this)
    @Inject
    lateinit var appPreference: AppPreference
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(appPreference.isNightMode()){
            setTheme(R.style.AppThemeNight)
        }
        setContentView(R.layout.activity_contributor)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = ""
        teams = ArrayList()
        teams.add(Team(1, "Abimbola Akinwonmi - Director"))
        teams.add(Team(2, "Micheal Akinwonmi"))
        teams.add(Team(3, "Victor Alonge"))
        teams.add(Team(4, "Olamide Onalaja"))
        teams.add(Team(5, "Oreoluwa Osundina"))
        //Log.e(TAG, "onCreate: " + teams)
        adapter = ContributorAdapter(this, teams)
        val listView = findViewById<ListView>(R.id.contri_view)
        listView.adapter = adapter
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    @SuppressLint("RestrictedApi")
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_fave -> {
                startActivity(Intent(this@ContributorActivity, FavoritesActivity::class.java))
                overridePendingTransition(R.anim.trans_right_in, R.anim.trans_right_out)
                return true
            }
            R.id.action_language -> {
                var v : View = findViewById(R.id.toolbar)
                val popup = PopupMenu(this, v, Gravity.END)
                popup.menuInflater.inflate(R.menu.lang_menu, popup.menu)
                popup.setOnMenuItemClickListener {
                    var language = 1
                    when(it.itemId){
                        R.id.action_english -> {
                            language = 1
                        }

                        R.id.action_yoruba -> {
                            language = 0
                        }

                    }

                    appPreference.setLanguage(language)
                    Snackbar.make(container,"Language Has Been Changed To ${if (language==1) "English" else "Yoruba"}", Snackbar.LENGTH_SHORT).show()
                    return@setOnMenuItemClickListener true
                }
                val menuHelper = MenuPopupHelper(this, popup.menu as MenuBuilder, v)
                menuHelper.setForceShowIcon(true)
                menuHelper.show()
            }
            R.id.action_night_mode ->{
                if(appPreference.isNightMode()){
                    appPreference.setNightMode(false)
                }else{
                    appPreference.setNightMode(true)
                }
                startActivity(intent)
                finish()            }

            else -> return super.onOptionsItemSelected(item)
        }
        return true
    }
}
