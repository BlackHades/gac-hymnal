package com.gacpedromediateam.primus.gachymnal.utils

import android.app.Activity
import android.content.Context
import android.net.ConnectivityManager

/**
 * Created by Primus on 7/15/2017.
 */

class NetworkHelper(var context: Context) {

    val isConnected: Boolean
        get() {
            return try {
                val connMgr = context.getSystemService(Activity.CONNECTIVITY_SERVICE) as ConnectivityManager
                val networkInfo = connMgr.activeNetworkInfo
                networkInfo != null && networkInfo.isConnected
            } catch (ex: Exception) {
                ex.printStackTrace()
                false
            }

        }
}
