package com.gacpedromediateam.primus.gachymnal.utils

import android.util.Log

import com.google.firebase.FirebaseApp
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.FirebaseInstanceIdService

/**
 * Created by micheal on 25/11/2017.
 */

class MyFirebaseInstanceID : FirebaseInstanceIdService() {
    var TAG = "InstanseID"
    override fun onTokenRefresh() {
        val refreshedToken = FirebaseInstanceId.getInstance().token
        Log.d(TAG, "Refreshed token: " + refreshedToken!!)
    }
}
