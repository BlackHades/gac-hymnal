package com.gacpedromediateam.primus.gachymnal.data.repository

import android.arch.lifecycle.LiveData
import com.gacpedromediateam.primus.gachymnal.data.model.*
import com.gacpedromediateam.primus.gachymnal.data.source.local.HymnDao
import com.gacpedromediateam.primus.gachymnal.data.source.local.VerseDao
import com.gacpedromediateam.primus.gachymnal.data.source.remote.HymnalApiService
import io.reactivex.Observable
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class HymnalRepository
@Inject
internal constructor(
        private val apiService: HymnalApiService,
        private val hymnDao: HymnDao,
        private val verseDao: VerseDao) {

    fun postEmail(email: String): Observable<Response<String>> {
        return apiService.sendEmail(email)
    }

    fun sendInstallationData(deviceData: DeviceData) : Observable<Response<String>>{
        return apiService.postInstallations(deviceData)
    }

    fun getMainHymn() : LiveData<ArrayList<Hymn>>{
        return hymnDao.mainHymn as LiveData<ArrayList<Hymn>>
    }

    fun getAppendixHymn():LiveData<ArrayList<Hymn>>{
        return hymnDao.getAppendixHymn() as LiveData<ArrayList<Hymn>>
    }

    fun getMainFavorite() : LiveData<ArrayList<Hymn>>{
        return hymnDao.mainFavoriteHymn as LiveData<ArrayList<Hymn>>
    }

    fun getAppendixFavorite():LiveData<ArrayList<Hymn>>{
        return hymnDao.appendixFavoriteHymn as LiveData<ArrayList<Hymn>>
    }

    fun updateMainFavorite(hymn: Hymn) : Int{
        return hymnDao.updateMainHymn(MainHymn().convertHymn(hymn))
    }

    fun getMainVerse(id: Int) : LiveData<ArrayList<Verse>>{
        return verseDao.getMainVerse(id.toLong()) as LiveData<ArrayList<Verse>>
    }

    fun getAppenidixVerse(id: Int) : LiveData<ArrayList<Verse>>{
        return verseDao.getAppendixVerse(id.toLong()) as LiveData<ArrayList<Verse>>
    }
}